import logo from "./logo.svg";
import "./App.css";
import Fruta from "./components/Fruta/Fruta.jsx";

// const allFruits = (fruits, index) => {
//   <div>{fruits}</div>;
// };

function App() {
  const fruits = [
    { name: "banana", color: "yellow", price: 6 },
    { name: "cherry", color: "red", price: 5 },
    { name: "strawberry", color: "red", price: 5 },
  ];

  return (
    <div className="App">
      <div id="all fruit names">
        <Fruta fruits={fruits}></Fruta>
      </div>
      <div id="red fruit names">
        <Fruta
          fruits={fruits.filter((fruit, index) => fruit.color.includes("red"))}
        ></Fruta>
      </div>
      <div id="total">
        {fruits.reduce(
          (acumulator, value, index, array) => (array[0].price += value.price)
        )}
      </div>
    </div>
  );
}

export default App;

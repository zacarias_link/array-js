import { Component } from "react";

// const { name } = this.props;

class Fruta extends Component {
  render() {
    return (
      <ul>
        {this.props.fruits.map((fruits, index) => (
          <li key={index}>fruit is {fruits.name} </li>
        ))}
      </ul>
    );
  }
}
export default Fruta;
